# CoAP Protocol Indication

This is the working area for the individual Internet-Draft, "CoAP Protocol Indication".

* [Datatracker Page](https://datatracker.ietf.org/doc/draft-amsuess-core-transport-indication)
* [Individual Draft](https://datatracker.ietf.org/doc/html/draft-amsuess-core-transport-indication)


## Contributing

See the
[guidelines for contributions](https://gitlab.com/chrysn/transport-indication/-/blob/master/CONTRIBUTING.md).

Contributions can be made by creating pull requests.
The GitLab interface supports creating pull requests using the Edit (✏) button.


## Command Line Usage

Formatted text and HTML versions of the draft can be built using `make`.

```sh
$ make
```

Command line usage requires that you have the necessary software installed.  See
[the instructions](https://github.com/martinthomson/i-d-template/blob/main/doc/SETUP.md).

